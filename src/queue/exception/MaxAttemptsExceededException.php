<?php

namespace toolmodules\queue\exception;

use RuntimeException;

class MaxAttemptsExceededException extends RuntimeException
{

}
