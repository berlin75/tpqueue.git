<?php
namespace toolmodules\queue\command;

use toolmodules\queue\Listener;
use toolmodules\queue\command\AbstractCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Listen extends AbstractCommand
{
    /** @var  Listener */
    protected $listener;

    protected static $defaultName = 'queue:listen';

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('connection', InputArgument::OPTIONAL, 'The name of the queue connection to work', null)
            ->addOption('queue', null, InputOption::VALUE_OPTIONAL, 'The queue to listen on', null)
            ->addOption('delay', null, InputOption::VALUE_OPTIONAL, 'Amount of time to delay failed jobs', 0)
            ->addOption('memory', null, InputOption::VALUE_OPTIONAL, 'The memory limit in megabytes', 128)
            ->addOption('timeout', null, InputOption::VALUE_OPTIONAL, 'Seconds a job may run before timing out', 60)
            ->addOption('sleep', null, InputOption::VALUE_OPTIONAL, 'Seconds to wait before checking queue for jobs', 3)
            ->addOption('tries', null, InputOption::VALUE_OPTIONAL, 'Number of times to attempt a job before logging it failed', 0)
            ->setDescription('Listen to a given queue');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $input->getArgument('connection') ?: 'redis';

        $queue   = $input->getOption('queue') ?: 'default';
        $delay   = $input->getOption('delay');
        $memory  = $input->getOption('memory');
        $timeout = $input->getOption('timeout');
        $sleep   = $input->getOption('sleep');
        $tries   = $input->getOption('tries');

        $this->listener = new Listener;
        $this->listener->setOutputHandler(function ($type, $line) use ($output) {
            $output->write($line);
        });

        $this->listener->listen($connection, $queue, $delay, $sleep, $tries, $memory, $timeout);
    }
}
